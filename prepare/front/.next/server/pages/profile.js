"use strict";
(() => {
var exports = {};
exports.id = 277;
exports.ids = [277];
exports.modules = {

/***/ 1999:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ profile),
  "getServerSideProps": () => (/* binding */ getServerSideProps)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(968);
var head_default = /*#__PURE__*/__webpack_require__.n(head_);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
var router_default = /*#__PURE__*/__webpack_require__.n(router_);
;// CONCATENATED MODULE: external "swr"
const external_swr_namespaceObject = require("swr");
var external_swr_default = /*#__PURE__*/__webpack_require__.n(external_swr_namespaceObject);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
// EXTERNAL MODULE: external "redux-saga"
var external_redux_saga_ = __webpack_require__(6537);
// EXTERNAL MODULE: external "antd"
var external_antd_ = __webpack_require__(5725);
// EXTERNAL MODULE: ./hooks/useInput.js
var useInput = __webpack_require__(3551);
// EXTERNAL MODULE: ./reducers/user.js
var reducers_user = __webpack_require__(8176);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./components/NicknameEditForm.js







const NicknameEditForm = function () {
  const {
    user
  } = (0,external_react_redux_.useSelector)(state => state.user);
  const [nickname, onChangeNickname] = (0,useInput/* default */.Z)((user === null || user === void 0 ? void 0 : user.nickname) || '');
  const dispatch = (0,external_react_redux_.useDispatch)();
  const onSubmit = (0,external_react_.useCallback)(() => {
    dispatch({
      type: reducers_user/* CHANGE_NICKNAME_REQUEST */.o,
      data: nickname
    });
  }, [nickname]);
  return /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Form, {
    style: {
      marginBottom: '20px',
      border: '1px solid #d9d9d9',
      padding: '20px'
    },
    children: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Input.Search, {
      value: nickname,
      onChange: onChangeNickname,
      addonBefore: "\uB2C9\uB124\uC784",
      enterButton: "\uC218\uC815",
      onSearch: onSubmit
    })
  });
};

/* harmony default export */ const components_NicknameEditForm = (NicknameEditForm);
// EXTERNAL MODULE: ./components/AppLayout.js + 2 modules
var AppLayout = __webpack_require__(1287);
// EXTERNAL MODULE: external "@ant-design/icons"
var icons_ = __webpack_require__(7066);
;// CONCATENATED MODULE: ./components/FollowList.js







const FollowList = function ({
  header,
  data,
  onClickMore,
  loading
}) {
  const dispatch = (0,external_react_redux_.useDispatch)();

  const onCancel = id => () => {
    if (header === '팔로잉 목록') {
      dispatch({
        type: reducers_user/* UNFOLLOW_REQUEST */.Bk,
        data: id
      });
    } else {
      dispatch({
        type: reducers_user/* REMOVE_FOLLOWER_REQUEST */.IB,
        data: id
      });
    }
  };

  return /*#__PURE__*/jsx_runtime_.jsx(external_antd_.List, {
    style: {
      marginBottom: '20px'
    },
    grid: {
      gutter: 4,
      xs: 2,
      md: 3
    },
    size: "small",
    header: /*#__PURE__*/jsx_runtime_.jsx("div", {
      children: header
    }),
    loadMore: /*#__PURE__*/jsx_runtime_.jsx("div", {
      style: {
        textAlign: 'center',
        margin: '10px 0'
      },
      children: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Button, {
        onClick: onClickMore,
        loading: loading,
        children: "\uB354 \uBCF4\uAE30"
      })
    }),
    bordered: true,
    dataSource: data,
    renderItem: item => /*#__PURE__*/jsx_runtime_.jsx(external_antd_.List.Item, {
      style: {
        marginTop: '20px'
      },
      children: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Card, {
        actions: [/*#__PURE__*/jsx_runtime_.jsx(icons_.StopOutlined, {
          onClick: onCancel(item.id)
        }, "stop")],
        children: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Card.Meta, {
          description: item.nickname
        })
      })
    })
  });
};

/* harmony default export */ const components_FollowList = (FollowList);
// EXTERNAL MODULE: ./store/configureStore.js + 4 modules
var configureStore = __webpack_require__(7675);
;// CONCATENATED MODULE: ./pages/profile.js















const fetcher = url => external_axios_default().get(url, {
  withCredentials: true
}).then(result => result.data);

const Profile = function () {
  const {
    user
  } = (0,external_react_redux_.useSelector)(state => state.user);
  const {
    0: followersLimit,
    1: setFollowersLimit
  } = (0,external_react_.useState)(3);
  const {
    0: followingsLimit,
    1: setFollowingsLimit
  } = (0,external_react_.useState)(3);
  const {
    data: followersData,
    error: followerError
  } = external_swr_default()(`http://localhost:3065/user/followers?limit=${followersLimit}`, fetcher);
  const {
    data: followingsData,
    error: followingError
  } = external_swr_default()(`http://localhost:3065/user/followings?limit=${followingsLimit}`, fetcher);
  (0,external_react_.useEffect)(() => {
    if (!(user && user.id)) {
      router_default().replace('/');
    }
  }, [user && user.id]);
  const loadMoreFollowings = (0,external_react_.useCallback)(() => {
    setFollowingsLimit(prev => prev + 3);
  }, []);
  const loadMoreFollowers = (0,external_react_.useCallback)(() => {
    setFollowersLimit(prev => prev + 3);
  }, []);

  if (!user) {
    return /*#__PURE__*/jsx_runtime_.jsx("div", {
      children: "\uB0B4 \uC815\uBCF4 \uB85C\uB529\uC911"
    });
  }

  if (followingError || followerError) {
    console.error(followingError || followerError);
    return /*#__PURE__*/jsx_runtime_.jsx("div", {
      children: "\uD314\uB85C\uC789/\uD314\uB85C\uC6CC \uB85C\uB4DC\uC911 \uC5D0\uB7EC\uBC1C\uC0DD"
    });
  }

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(AppLayout/* default */.Z, {
    children: [/*#__PURE__*/jsx_runtime_.jsx((head_default()), {
      children: /*#__PURE__*/jsx_runtime_.jsx("title", {
        children: "\uB0B4 \uD504\uB85C\uD544 | NodeBird"
      })
    }), /*#__PURE__*/jsx_runtime_.jsx(components_NicknameEditForm, {}), /*#__PURE__*/jsx_runtime_.jsx(components_FollowList, {
      header: "\uD314\uB85C\uC789 \uBAA9\uB85D",
      data: followingsData,
      onClickMore: loadMoreFollowings,
      loading: !followingsData && followingError
    }), /*#__PURE__*/jsx_runtime_.jsx(components_FollowList, {
      header: "\uD314\uB85C\uC6CC \uBAA9\uB85D",
      data: followersData,
      onClickMore: loadMoreFollowers,
      loading: !followersData && followerError
    })]
  });
};

const getServerSideProps = configureStore/* default.getServerSideProps */.Z.getServerSideProps(async context => {
  const cookie = context.req ? context.req.headers.cookie : '';
  (external_axios_default()).defaults.headers.Cookie = '';

  if (context.req && cookie) {
    (external_axios_default()).defaults.headers.Cookie = cookie;
  }

  context.store.dispatch({
    type: reducers_user/* LOAD_MY_INFO_REQUEST */.qq
  });
  context.store.dispatch(external_redux_saga_.END);
  await context.store.sagaTask.toPromise();
});
/* harmony default export */ const profile = (Profile);

/***/ }),

/***/ 7066:
/***/ ((module) => {

module.exports = require("@ant-design/icons");

/***/ }),

/***/ 5725:
/***/ ((module) => {

module.exports = require("antd");

/***/ }),

/***/ 2167:
/***/ ((module) => {

module.exports = require("axios");

/***/ }),

/***/ 7133:
/***/ ((module) => {

module.exports = require("immer");

/***/ }),

/***/ 5648:
/***/ ((module) => {

module.exports = require("next-redux-wrapper");

/***/ }),

/***/ 562:
/***/ ((module) => {

module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 968:
/***/ ((module) => {

module.exports = require("next/head");

/***/ }),

/***/ 1853:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 6022:
/***/ ((module) => {

module.exports = require("react-redux");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 6695:
/***/ ((module) => {

module.exports = require("redux");

/***/ }),

/***/ 173:
/***/ ((module) => {

module.exports = require("redux-devtools-extension");

/***/ }),

/***/ 6537:
/***/ ((module) => {

module.exports = require("redux-saga");

/***/ }),

/***/ 6477:
/***/ ((module) => {

module.exports = require("redux-saga/effects");

/***/ }),

/***/ 7518:
/***/ ((module) => {

module.exports = require("styled-components");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [400,664,675,287], () => (__webpack_exec__(1999)));
module.exports = __webpack_exports__;

})();