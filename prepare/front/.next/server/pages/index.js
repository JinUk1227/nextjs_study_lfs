"use strict";
(() => {
var exports = {};
exports.id = 405;
exports.ids = [405];
exports.modules = {

/***/ 6140:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": () => (/* binding */ pages),
  "getServerSideProps": () => (/* binding */ getServerSideProps)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
// EXTERNAL MODULE: external "redux-saga"
var external_redux_saga_ = __webpack_require__(6537);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
// EXTERNAL MODULE: external "antd"
var external_antd_ = __webpack_require__(5725);
// EXTERNAL MODULE: ./reducers/post.js
var post = __webpack_require__(3075);
// EXTERNAL MODULE: ./hooks/useInput.js
var useInput = __webpack_require__(3551);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./components/PostForm.js








const PostForm = function () {
  const {
    imagePaths,
    addPostDone,
    addPostLoading
  } = (0,external_react_redux_.useSelector)(state => state.post);
  const [text, onChangeText, setText] = (0,useInput/* default */.Z)('');
  const dispatch = (0,external_react_redux_.useDispatch)();
  const imageInput = (0,external_react_.useRef)();
  const onClickImageUpload = (0,external_react_.useCallback)(() => {
    imageInput.current.click();
  }, [imageInput.current]);
  (0,external_react_.useEffect)(() => {
    if (addPostDone) {
      setText('');
    }
  }, [addPostDone]);
  const onSubmit = (0,external_react_.useCallback)(() => {
    if (!text || !text.trim()) {
      return alert('게시글을 작성하세요.');
    }

    const formData = new FormData();
    imagePaths.forEach(p => {
      formData.append('image', p);
    });
    formData.append('content', text);
    return dispatch({
      type: post/* ADD_POST_REQUEST */.z9,
      data: formData
    });
  }, [text, imagePaths]);
  const onChangeImages = (0,external_react_.useCallback)(e => {
    const imageFormData = new FormData();
    [].forEach.call(e.target.files, f => {
      imageFormData.append('image', f);
    });
    dispatch({
      type: post/* UPLOAD_IMAGES_REQUEST */.QA,
      data: imageFormData
    });
  }, []);
  const onRemoveImage = (0,external_react_.useCallback)(index => () => {
    dispatch({
      type: post/* REMOVE_IMAGE */.Po,
      data: index
    });
  });
  return (
    /*#__PURE__*/
    // eslint-disable-next-line react/jsx-filename-extension
    (0,jsx_runtime_.jsxs)(external_antd_.Form, {
      style: {
        margin: '10px 0 20px'
      },
      encType: "multipart/form-data",
      onFinish: onSubmit,
      children: [/*#__PURE__*/jsx_runtime_.jsx(external_antd_.Input.TextArea, {
        value: text,
        onChange: onChangeText,
        maxLength: 140,
        placeholder: "\uC5B4\uB5A4 \uC2E0\uAE30\uD55C \uC77C\uC774 \uC788\uC5C8\uB098\uC694?"
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        children: [/*#__PURE__*/jsx_runtime_.jsx("input", {
          type: "file",
          name: "image",
          multiple: true,
          hidden: true,
          ref: imageInput,
          onChange: onChangeImages
        }), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Button, {
          onClick: onClickImageUpload,
          children: "\uC774\uBBF8\uC9C0 \uC5C5\uB85C\uB4DC"
        }), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Button, {
          type: "primary",
          style: {
            float: 'right'
          },
          htmlType: "submit",
          loading: addPostLoading,
          children: "\uC9F9\uC9F9"
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        children: imagePaths.map((v, i) => /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          style: {
            display: 'inline-block'
          },
          children: [/*#__PURE__*/jsx_runtime_.jsx("img", {
            src: `http://localhost:3065/${v}`,
            style: {
              width: '200px'
            },
            alt: v
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            children: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Button, {
              onClick: onRemoveImage(i),
              children: "\uC81C\uAC70"
            })
          })]
        }, v))
      })]
    })
  );
};

/* harmony default export */ const components_PostForm = (PostForm);
// EXTERNAL MODULE: ./components/PostCard.js + 6 modules
var PostCard = __webpack_require__(9160);
// EXTERNAL MODULE: ./components/AppLayout.js + 2 modules
var AppLayout = __webpack_require__(1287);
// EXTERNAL MODULE: ./reducers/user.js
var user = __webpack_require__(8176);
// EXTERNAL MODULE: ./store/configureStore.js + 4 modules
var configureStore = __webpack_require__(7675);
;// CONCATENATED MODULE: ./pages/index.js













const Home = function () {
  const dispatch = (0,external_react_redux_.useDispatch)();
  const user = (0,external_react_redux_.useSelector)(state => state.user.user);
  const {
    mainPosts,
    hasMorePost,
    loadPostsLoading,
    retweetError
  } = (0,external_react_redux_.useSelector)(state => state.post);
  (0,external_react_.useEffect)(() => {
    if (retweetError) {
      alert(retweetError);
    }
  }, [retweetError]);
  (0,external_react_.useEffect)(() => {
    function onScroll() {
      if (window.pageYOffset + document.documentElement.clientHeight > document.documentElement.scrollHeight - 300) {
        if (hasMorePost && !loadPostsLoading) {
          var _mainPosts;

          const lastId = (_mainPosts = mainPosts[mainPosts.length - 1]) === null || _mainPosts === void 0 ? void 0 : _mainPosts.id;
          dispatch({
            type: post/* LOAD_POSTS_REQUEST */.aO,
            lastId
          });
        }
      }
    }

    window.addEventListener('scroll', onScroll);
    return () => {
      window.removeEventListener('scroll', onScroll);
    };
  }, [hasMorePost, loadPostsLoading, mainPosts]);
  return (
    /*#__PURE__*/
    // eslint-disable-next-line react/jsx-filename-extension
    (0,jsx_runtime_.jsxs)(AppLayout/* default */.Z, {
      children: [user && /*#__PURE__*/jsx_runtime_.jsx(components_PostForm, {}), mainPosts.map(c => /*#__PURE__*/jsx_runtime_.jsx(PostCard/* default */.Z, {
        post: c
      }, c.id))]
    })
  );
};

const getServerSideProps = configureStore/* default.getServerSideProps */.Z.getServerSideProps(async context => {
  const cookie = context.req ? context.req.headers.cookie : '';
  (external_axios_default()).defaults.headers.Cookie = '';

  if (context.req && cookie) {
    (external_axios_default()).defaults.headers.Cookie = cookie;
  }

  context.store.dispatch({
    type: user/* LOAD_MY_INFO_REQUEST */.qq
  });
  context.store.dispatch({
    type: post/* LOAD_POSTS_REQUEST */.aO
  });
  context.store.dispatch(external_redux_saga_.END);
  await context.store.sagaTask.toPromise();
});
/* harmony default export */ const pages = (Home);

/***/ }),

/***/ 7066:
/***/ ((module) => {

module.exports = require("@ant-design/icons");

/***/ }),

/***/ 5725:
/***/ ((module) => {

module.exports = require("antd");

/***/ }),

/***/ 2167:
/***/ ((module) => {

module.exports = require("axios");

/***/ }),

/***/ 1635:
/***/ ((module) => {

module.exports = require("dayjs");

/***/ }),

/***/ 7133:
/***/ ((module) => {

module.exports = require("immer");

/***/ }),

/***/ 5648:
/***/ ((module) => {

module.exports = require("next-redux-wrapper");

/***/ }),

/***/ 562:
/***/ ((module) => {

module.exports = require("next/dist/server/denormalize-page-path.js");

/***/ }),

/***/ 4014:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/i18n/normalize-locale-path.js");

/***/ }),

/***/ 8020:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/mitt.js");

/***/ }),

/***/ 4964:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router-context.js");

/***/ }),

/***/ 9565:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-asset-path-from-route.js");

/***/ }),

/***/ 4365:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/get-middleware-regex.js");

/***/ }),

/***/ 1428:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/is-dynamic.js");

/***/ }),

/***/ 1292:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/parse-relative-url.js");

/***/ }),

/***/ 979:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/querystring.js");

/***/ }),

/***/ 6052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/resolve-rewrites.js");

/***/ }),

/***/ 4226:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-matcher.js");

/***/ }),

/***/ 5052:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/router/utils/route-regex.js");

/***/ }),

/***/ 9232:
/***/ ((module) => {

module.exports = require("next/dist/shared/lib/utils.js");

/***/ }),

/***/ 1853:
/***/ ((module) => {

module.exports = require("next/router");

/***/ }),

/***/ 6689:
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ 6022:
/***/ ((module) => {

module.exports = require("react-redux");

/***/ }),

/***/ 8096:
/***/ ((module) => {

module.exports = require("react-slick");

/***/ }),

/***/ 997:
/***/ ((module) => {

module.exports = require("react/jsx-runtime");

/***/ }),

/***/ 6695:
/***/ ((module) => {

module.exports = require("redux");

/***/ }),

/***/ 173:
/***/ ((module) => {

module.exports = require("redux-devtools-extension");

/***/ }),

/***/ 6537:
/***/ ((module) => {

module.exports = require("redux-saga");

/***/ }),

/***/ 6477:
/***/ ((module) => {

module.exports = require("redux-saga/effects");

/***/ }),

/***/ 7518:
/***/ ((module) => {

module.exports = require("styled-components");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, [400,664,675,287,160], () => (__webpack_exec__(6140)));
module.exports = __webpack_exports__;

})();