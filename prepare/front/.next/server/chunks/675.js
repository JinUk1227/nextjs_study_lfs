"use strict";
exports.id = 675;
exports.ids = [675];
exports.modules = {

/***/ 3075:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "aO": () => (/* binding */ LOAD_POSTS_REQUEST),
/* harmony export */   "ZP": () => (/* binding */ LOAD_POSTS_SUCCESS),
/* harmony export */   "T5": () => (/* binding */ LOAD_POSTS_FAILURE),
/* harmony export */   "x5": () => (/* binding */ LOAD_USER_POSTS_REQUEST),
/* harmony export */   "Ag": () => (/* binding */ LOAD_USER_POSTS_SUCCESS),
/* harmony export */   "N3": () => (/* binding */ LOAD_USER_POSTS_FAILURE),
/* harmony export */   "az": () => (/* binding */ LOAD_HASHTAG_POSTS_REQUEST),
/* harmony export */   "LF": () => (/* binding */ LOAD_HASHTAG_POSTS_SUCCESS),
/* harmony export */   "uk": () => (/* binding */ LOAD_HASHTAG_POSTS_FAILURE),
/* harmony export */   "EG": () => (/* binding */ LOAD_POST_REQUEST),
/* harmony export */   "YQ": () => (/* binding */ LOAD_POST_SUCCESS),
/* harmony export */   "rl": () => (/* binding */ LOAD_POST_FAILURE),
/* harmony export */   "z9": () => (/* binding */ ADD_POST_REQUEST),
/* harmony export */   "_s": () => (/* binding */ ADD_POST_SUCCESS),
/* harmony export */   "tP": () => (/* binding */ ADD_POST_FAILURE),
/* harmony export */   "HU": () => (/* binding */ REMOVE_POST_REQUEST),
/* harmony export */   "rK": () => (/* binding */ REMOVE_POST_SUCCESS),
/* harmony export */   "Ws": () => (/* binding */ REMOVE_POST_FAILURE),
/* harmony export */   "Ot": () => (/* binding */ ADD_COMMENT_REQUEST),
/* harmony export */   "nv": () => (/* binding */ ADD_COMMENT_SUCCESS),
/* harmony export */   "rX": () => (/* binding */ ADD_COMMENT_FAILURE),
/* harmony export */   "xD": () => (/* binding */ LIKE_POST_REQUEST),
/* harmony export */   "Gz": () => (/* binding */ LIKE_POST_SUCCESS),
/* harmony export */   "cT": () => (/* binding */ LIKE_POST_FAILURE),
/* harmony export */   "VT": () => (/* binding */ UNLIKE_POST_REQUEST),
/* harmony export */   "XD": () => (/* binding */ UNLIKE_POST_SUCCESS),
/* harmony export */   "gq": () => (/* binding */ UNLIKE_POST_FAILURE),
/* harmony export */   "QA": () => (/* binding */ UPLOAD_IMAGES_REQUEST),
/* harmony export */   "kv": () => (/* binding */ UPLOAD_IMAGES_SUCCESS),
/* harmony export */   "hX": () => (/* binding */ UPLOAD_IMAGES_FAILURE),
/* harmony export */   "a0": () => (/* binding */ RETWEET_REQUEST),
/* harmony export */   "ZL": () => (/* binding */ RETWEET_SUCCESS),
/* harmony export */   "FM": () => (/* binding */ RETWEET_FAILURE),
/* harmony export */   "Po": () => (/* binding */ REMOVE_IMAGE),
/* harmony export */   "x6": () => (/* binding */ ADD_POST_TO_ME),
/* harmony export */   "KK": () => (/* binding */ REMOVE_POST_OF_ME),
/* harmony export */   "Co": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* unused harmony exports initialState, addPost, addComment */
/* harmony import */ var immer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7133);
/* harmony import */ var immer__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(immer__WEBPACK_IMPORTED_MODULE_0__);

const initialState = {
  mainPosts: [],
  imagePaths: [],
  singlePost: null,
  hasMorePost: true,
  loadPostsLoading: false,
  loadPostsDone: false,
  loadPostsError: null,
  loadPostLoading: false,
  loadPostDone: false,
  loadPostError: null,
  addPostLoading: false,
  addPostDone: false,
  addPostError: null,
  removePostLoading: false,
  removePostDone: false,
  removePostError: null,
  addCommentLoading: false,
  addCommentDone: false,
  addCommentError: null,
  likePostLoading: false,
  likePostDone: false,
  likePostError: null,
  unlikePostLoading: false,
  unlikePostDone: false,
  unlikePostError: null,
  uploadImagesLoading: false,
  uploadImagesDone: false,
  uploadImagesError: null,
  removeImageLoading: false,
  removeImageDone: false,
  removeImageError: null,
  retweetLoading: false,
  retweetDone: false,
  retweetError: null
};
initialState.mainPosts = initialState.mainPosts.concat();
const LOAD_POSTS_REQUEST = 'LOAD_POSTS_REQUEST';
const LOAD_POSTS_SUCCESS = 'LOAD_POSTS_SUCCESS';
const LOAD_POSTS_FAILURE = 'LOAD_POSTS_FAILURE';
const LOAD_USER_POSTS_REQUEST = 'LOAD_USER_POSTS_REQUEST';
const LOAD_USER_POSTS_SUCCESS = 'LOAD_USER_POSTS_SUCCESS';
const LOAD_USER_POSTS_FAILURE = 'LOAD_USER_POSTS_FAILURE';
const LOAD_HASHTAG_POSTS_REQUEST = 'LOAD_HASHTAG_POSTS_REQUEST';
const LOAD_HASHTAG_POSTS_SUCCESS = 'LOAD_HASHTAG_POSTS_SUCCESS';
const LOAD_HASHTAG_POSTS_FAILURE = 'LOAD_HASHTAG_POSTS_FAILURE';
const LOAD_POST_REQUEST = 'LOAD_POST_REQUEST';
const LOAD_POST_SUCCESS = 'LOAD_POST_SUCCESS';
const LOAD_POST_FAILURE = 'LOAD_POST_FAILURE';
const ADD_POST_REQUEST = 'ADD_POST_REQUEST';
const ADD_POST_SUCCESS = 'ADD_POST_SUCCESS';
const ADD_POST_FAILURE = 'ADD_POST_FAILURE';
const REMOVE_POST_REQUEST = 'REMOVE_POST_REQUEST';
const REMOVE_POST_SUCCESS = 'REMOVE_POST_SUCCESS';
const REMOVE_POST_FAILURE = 'REMOVE_POST_FAILURE';
const ADD_COMMENT_REQUEST = 'ADD_COMMENT_REQUEST';
const ADD_COMMENT_SUCCESS = 'ADD_COMMENT_SUCCESS';
const ADD_COMMENT_FAILURE = 'ADD_COMMENT_FAILURE';
const LIKE_POST_REQUEST = 'LIKE_POST_REQUEST';
const LIKE_POST_SUCCESS = 'LIKE_POST_SUCCESS';
const LIKE_POST_FAILURE = 'LIKE_POST_FAILURE';
const UNLIKE_POST_REQUEST = 'UNLIKE_POST_REQUEST';
const UNLIKE_POST_SUCCESS = 'UNLIKE_POST_SUCCESS';
const UNLIKE_POST_FAILURE = 'UNLIKE_POST_FAILURE';
const UPLOAD_IMAGES_REQUEST = 'UPLOAD_IMAGES_REQUEST';
const UPLOAD_IMAGES_SUCCESS = 'UPLOAD_IMAGES_SUCCESS';
const UPLOAD_IMAGES_FAILURE = 'UPLOAD_IMAGES_FAILURE';
const RETWEET_REQUEST = 'RETWEET_REQUEST';
const RETWEET_SUCCESS = 'RETWEET_SUCCESS';
const RETWEET_FAILURE = 'RETWEET_FAILURE';
const REMOVE_IMAGE = 'REMOVE_IMAGE';
const ADD_POST_TO_ME = 'ADD_POST_TO_ME';
const REMOVE_POST_OF_ME = 'REMOVE_POST_OF_ME';
const addPost = data => ({
  type: ADD_POST_REQUEST,
  data
});
const addComment = data => ({
  type: ADD_COMMENT_REQUEST,
  data
}); // eslint-disable-next-line default-param-last

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((state = initialState, action) => immer__WEBPACK_IMPORTED_MODULE_0___default()(state, draft => {
  switch (action.type) {
    case LOAD_POST_REQUEST:
      draft.loadPostLoading = true;
      draft.loadPostDone = false;
      draft.loadPostError = null;
      break;

    case LOAD_POST_SUCCESS:
      draft.loadPostLoading = false;
      draft.loadPostDone = true;
      draft.singlePost = action.data;
      break;

    case LOAD_POST_FAILURE:
      draft.loadPostLoading = false;
      draft.loadPostError = action.error;
      break;

    case LOAD_USER_POSTS_REQUEST:
    case LOAD_POSTS_REQUEST:
    case LOAD_HASHTAG_POSTS_REQUEST:
      draft.loadPostsLoading = true;
      draft.loadPostsDone = false;
      draft.loadPostsError = null;
      break;

    case LOAD_USER_POSTS_SUCCESS:
    case LOAD_HASHTAG_POSTS_SUCCESS:
    case LOAD_POSTS_SUCCESS:
      draft.loadPostsLoading = false;
      draft.loadPostsDone = true;
      draft.mainPosts = draft.mainPosts.concat(action.data);
      draft.hasMorePosts = action.data.length === 10;
      break;

    case LOAD_USER_POSTS_FAILURE:
    case LOAD_HASHTAG_POSTS_FAILURE:
    case LOAD_POSTS_FAILURE:
      draft.loadPostsLoading = false;
      draft.loadPostsError = action.error;
      break;

    case ADD_POST_REQUEST:
      {
        draft.addPostLoading = true;
        draft.addPostDone = false;
        draft.addPostError = null;
        break;
      }

    case ADD_POST_SUCCESS:
      {
        draft.addPostLoading = false;
        draft.addPostDone = true;
        draft.mainPosts.unshift(action.data);
        draft.imagePaths = [];
        break;
      }

    case ADD_POST_FAILURE:
      draft.addPostLoading = false;
      draft.addPostError = action.error;
      break;

    case REMOVE_POST_REQUEST:
      draft.removePostLoading = true;
      draft.removePostDone = false;
      draft.removePostError = null;
      break;

    case REMOVE_POST_SUCCESS:
      draft.removePostLoading = false;
      draft.removePostDone = true;
      draft.mainPosts = draft.mainPosts.filter(v => v.id !== action.data.PostId);
      break;

    case REMOVE_POST_FAILURE:
      draft.removePostLoading = false;
      draft.removePostError = action.error;
      break;

    case ADD_COMMENT_REQUEST:
      draft.addCommentLoading = true;
      draft.addCommentDone = false;
      draft.addCommentError = null;
      break;

    case ADD_COMMENT_SUCCESS:
      {
        const post = draft.mainPosts.find(v => v.id === action.data.PostId);
        post.Comments.unshift(action.data);
        draft.addCommentLoading = false;
        draft.addCommentDone = true;
        break;
      }

    case ADD_COMMENT_FAILURE:
      draft.addCommentLoading = false;
      draft.addCommentError = action.error;
      break;

    case LIKE_POST_REQUEST:
      draft.likePostLoading = true;
      draft.likePostDone = false;
      draft.likePostError = null;
      break;

    case LIKE_POST_SUCCESS:
      {
        const post = draft.mainPosts.find(v => v.id === action.data.PostId);
        post.Likers.push({
          id: action.data.UserId
        });
        draft.likePostLoading = false;
        draft.likePostDone = true;
        break;
      }

    case LIKE_POST_FAILURE:
      draft.likePostLoading = false;
      draft.likePostError = action.error;
      break;

    case UNLIKE_POST_REQUEST:
      draft.unlikePostLoading = true;
      draft.unlikePostDone = false;
      draft.unlikePostError = null;
      break;

    case UNLIKE_POST_SUCCESS:
      {
        const post = draft.mainPosts.find(v => v.id === action.data.PostId);
        post.Likers = post.Likers.filter(v => v.id !== action.data.UserId);
        draft.unlikePostLoading = false;
        draft.unlikePostDone = true;
        break;
      }

    case UNLIKE_POST_FAILURE:
      draft.unlikePostLoading = false;
      draft.unlikePostError = action.error;
      break;

    case UPLOAD_IMAGES_REQUEST:
      draft.uploadImagesLoading = true;
      draft.uploadImagesDone = false;
      draft.uploadImagesError = null;
      break;

    case UPLOAD_IMAGES_SUCCESS:
      {
        draft.imagePaths = action.data;
        draft.uploadImagesLoading = false;
        draft.uploadImagesDone = true;
        break;
      }

    case UPLOAD_IMAGES_FAILURE:
      draft.uploadImagesLoading = false;
      draft.uploadImagesError = action.error;
      break;

    case RETWEET_REQUEST:
      draft.retweetLoading = true;
      draft.retweetDone = false;
      draft.retweetError = null;
      break;

    case RETWEET_SUCCESS:
      {
        draft.mainPosts.unshift(action.data);
        draft.retweetLoading = false;
        draft.retweetDone = true;
        break;
      }

    case RETWEET_FAILURE:
      draft.retweetLoading = false;
      draft.retweetError = action.error;
      break;

    case REMOVE_IMAGE:
      draft.imagePaths = draft.imagePaths.filter((v, i) => i !== action.data);
      break;

    default:
      break;
  }
}));

/***/ }),

/***/ 8176:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "qq": () => (/* binding */ LOAD_MY_INFO_REQUEST),
/* harmony export */   "LJ": () => (/* binding */ LOAD_MY_INFO_SUCCESS),
/* harmony export */   "Cq": () => (/* binding */ LOAD_MY_INFO_FAILURE),
/* harmony export */   "dQ": () => (/* binding */ LOAD_USER_REQUEST),
/* harmony export */   "DU": () => (/* binding */ LOAD_USER_SUCCESS),
/* harmony export */   "Ls": () => (/* binding */ LOAD_USER_FAILURE),
/* harmony export */   "uF": () => (/* binding */ LOG_IN_REQUEST),
/* harmony export */   "RZ": () => (/* binding */ LOG_IN_SUCCESS),
/* harmony export */   "yK": () => (/* binding */ LOG_IN_FAILURE),
/* harmony export */   "Oy": () => (/* binding */ LOG_OUT_REQUEST),
/* harmony export */   "kV": () => (/* binding */ LOG_OUT_SUCCESS),
/* harmony export */   "mD": () => (/* binding */ LOG_OUT_FAILURE),
/* harmony export */   "pK": () => (/* binding */ SIGN_UP_REQUEST),
/* harmony export */   "I": () => (/* binding */ SIGN_UP_SUCCESS),
/* harmony export */   "bP": () => (/* binding */ SIGN_UP_FAILURE),
/* harmony export */   "o": () => (/* binding */ CHANGE_NICKNAME_REQUEST),
/* harmony export */   "dr": () => (/* binding */ CHANGE_NICKNAME_SUCCESS),
/* harmony export */   "PG": () => (/* binding */ CHANGE_NICKNAME_FAILURE),
/* harmony export */   "U_": () => (/* binding */ FOLLOW_REQUEST),
/* harmony export */   "mv": () => (/* binding */ FOLLOW_SUCCESS),
/* harmony export */   "DG": () => (/* binding */ FOLLOW_FAILURE),
/* harmony export */   "Bk": () => (/* binding */ UNFOLLOW_REQUEST),
/* harmony export */   "D7": () => (/* binding */ UNFOLLOW_SUCCESS),
/* harmony export */   "OR": () => (/* binding */ UNFOLLOW_FAILURE),
/* harmony export */   "G$": () => (/* binding */ LOAD_FOLLOWERS_REQUEST),
/* harmony export */   "Y6": () => (/* binding */ LOAD_FOLLOWERS_SUCCESS),
/* harmony export */   "vT": () => (/* binding */ LOAD_FOLLOWERS_FAILURE),
/* harmony export */   "Xd": () => (/* binding */ LOAD_FOLLOWINGS_REQUEST),
/* harmony export */   "IQ": () => (/* binding */ LOAD_FOLLOWINGS_SUCCESS),
/* harmony export */   "qt": () => (/* binding */ LOAD_FOLLOWINGS_FAILURE),
/* harmony export */   "IB": () => (/* binding */ REMOVE_FOLLOWER_REQUEST),
/* harmony export */   "pe": () => (/* binding */ REMOVE_FOLLOWER_SUCCESS),
/* harmony export */   "IK": () => (/* binding */ REMOVE_FOLLOWER_FAILURE),
/* harmony export */   "vR": () => (/* binding */ logoutRequestAction),
/* harmony export */   "ZP": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* unused harmony exports initialState, signUpAction, signUpSuccess, loginRequestAction, signUp */
/* harmony import */ var immer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7133);
/* harmony import */ var immer__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(immer__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _post__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3075);


const initialState = {
  loadMyInfoLoading: false,
  loadMyInfoDone: false,
  loadMyInfoError: null,
  loadUserLoading: false,
  loadUserDone: false,
  loadUserError: null,
  logInLoading: false,
  logInDone: false,
  logInError: null,
  logOutLoading: false,
  logOutDone: false,
  logOutError: null,
  signUpLoading: false,
  signUpDone: false,
  signUpError: null,
  changeNicknameLoading: false,
  changeNicknameDone: false,
  changeNicknameError: null,
  followingLoading: false,
  followingDone: false,
  followingError: null,
  unfollowingLoading: false,
  unfollowingDone: false,
  unfollowingError: null,
  loadFollowersLoading: false,
  loadFollowersDone: false,
  loadFollowersError: null,
  loadFollowingsLoading: false,
  loadFollowingsDone: false,
  loadFollowingsError: null,
  removeFollowerLoading: false,
  removeFollowerDone: false,
  removeFollowerError: null,
  postId: null,
  user: null,
  userInfo: null,
  signUpData: {},
  loginData: {}
};
const LOAD_MY_INFO_REQUEST = 'LOAD_MY_INFO_REQUEST';
const LOAD_MY_INFO_SUCCESS = 'LOAD_MY_INFO_SUCCESS';
const LOAD_MY_INFO_FAILURE = 'LOAD_MY_INFO_FAILURE';
const LOAD_USER_REQUEST = 'LOAD_USER_REQUEST';
const LOAD_USER_SUCCESS = 'LOAD_USER_SUCCESS';
const LOAD_USER_FAILURE = 'LOAD_USER_FAILURE';
const LOG_IN_REQUEST = 'LOG_IN_REQUEST';
const LOG_IN_SUCCESS = 'LOG_IN_SUCCESS';
const LOG_IN_FAILURE = 'LOG_IN_FAILURE';
const LOG_OUT_REQUEST = 'LOG_OUT_REQUEST';
const LOG_OUT_SUCCESS = 'LOG_OUT_SUCCESS';
const LOG_OUT_FAILURE = 'LOG_OUT_FAILURE';
const SIGN_UP_REQUEST = 'SIGN_UP_REQUEST';
const SIGN_UP_SUCCESS = 'SIGN_UP_SUCCESS';
const SIGN_UP_FAILURE = 'SIGN_UP_FAILURE';
const CHANGE_NICKNAME_REQUEST = 'CHANGE_NICKNAME_REQUEST';
const CHANGE_NICKNAME_SUCCESS = 'CHANGE_NICKNAME_SUCCESS';
const CHANGE_NICKNAME_FAILURE = 'CHANGE_NICKNAME_FAILURE';
const FOLLOW_REQUEST = 'FOLLOW_REQUEST';
const FOLLOW_SUCCESS = 'FOLLOW_SUCCESS';
const FOLLOW_FAILURE = 'FOLLOW_FAILURE';
const UNFOLLOW_REQUEST = 'UNFOLLOW_REQUEST';
const UNFOLLOW_SUCCESS = 'UNFOLLOW_SUCCESS';
const UNFOLLOW_FAILURE = 'UNFOLLOW_FAILURE';
const LOAD_FOLLOWERS_REQUEST = 'LOAD_FOLLOWERS_REQUEST';
const LOAD_FOLLOWERS_SUCCESS = 'LOAD_FOLLOWERS_SUCCESS';
const LOAD_FOLLOWERS_FAILURE = 'LOAD_FOLLOWERS_FAILURE';
const LOAD_FOLLOWINGS_REQUEST = 'LOAD_FOLLOWINGS_REQUEST';
const LOAD_FOLLOWINGS_SUCCESS = 'LOAD_FOLLOWINGS_SUCCESS';
const LOAD_FOLLOWINGS_FAILURE = 'LOAD_FOLLOWINGS_FAILURE';
const REMOVE_FOLLOWER_REQUEST = 'REMOVE_FOLLOWER_REQUEST';
const REMOVE_FOLLOWER_SUCCESS = 'REMOVE_FOLLOWER_SUCCESS';
const REMOVE_FOLLOWER_FAILURE = 'REMOVE_FOLLOWER_FAILURE';
const signUpAction = data => ({
  type: SIGN_UP_REQUEST,
  data
});
const signUpSuccess = {
  type: SIGN_UP_SUCCESS
};
const loginRequestAction = data => ({
  type: LOG_IN_REQUEST,
  data
});
const logoutRequestAction = {
  type: LOG_OUT_REQUEST
};
const signUp = data => ({
  type: SIGN_UP_REQUEST,
  data
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((state = initialState, action) => immer__WEBPACK_IMPORTED_MODULE_0___default()(state, draft => {
  switch (action.type) {
    case LOAD_USER_REQUEST:
      draft.loadUserLoading = true;
      draft.loadUserError = null;
      draft.loadUserDone = false;
      break;

    case LOAD_USER_SUCCESS:
      draft.loadUserLoading = false;
      draft.userInfo = action.data;
      draft.loadUserDone = true;
      break;

    case LOAD_USER_FAILURE:
      draft.loadUserLoading = false;
      draft.loadUserError = action.error;
      break;

    case LOAD_MY_INFO_REQUEST:
      draft.loadMyInfoLoading = true;
      draft.loadMyInfoError = null;
      draft.loadMyInfoDone = false;
      break;

    case LOAD_MY_INFO_SUCCESS:
      draft.loadMyInfoLoading = false;
      draft.user = action.data;
      draft.loadMyInfoDone = true;
      break;

    case LOAD_MY_INFO_FAILURE:
      draft.loadMyInfoLoading = false;
      draft.loadMyInfoError = action.error;
      break;

    case LOG_IN_REQUEST:
      draft.logInLoading = true;
      draft.logInError = null;
      draft.logInDone = false;
      break;

    case LOG_IN_SUCCESS:
      draft.logInLoading = false;
      draft.user = action.data;
      draft.logInDone = true;
      break;

    case LOG_IN_FAILURE:
      draft.logInLoading = false;
      draft.logInError = action.error;
      break;

    case LOG_OUT_REQUEST:
      draft.logOutLoading = true;
      draft.logOutError = null;
      draft.logOutDone = false;
      break;

    case LOG_OUT_SUCCESS:
      draft.logOutLoading = false;
      draft.logOutDone = true;
      draft.user = null;
      break;

    case LOG_OUT_FAILURE:
      draft.logOutLoading = false;
      draft.logOutError = action.error;
      break;

    case SIGN_UP_REQUEST:
      draft.signUpLoading = true;
      draft.signUpError = null;
      draft.signUpDone = false;
      break;

    case SIGN_UP_SUCCESS:
      draft.signUpLoading = false;
      draft.signUpDone = true;
      break;

    case SIGN_UP_FAILURE:
      draft.signUpLoading = false;
      draft.signUpError = action.error;
      break;

    case CHANGE_NICKNAME_REQUEST:
      draft.changeNicknameLoading = true;
      draft.changeNicknameError = null;
      draft.changeNicknameDone = false;
      break;

    case CHANGE_NICKNAME_SUCCESS:
      draft.user.nickname = action.data.nickname;
      draft.changeNicknameLoading = false;
      draft.changeNicknameDone = true;
      break;

    case CHANGE_NICKNAME_FAILURE:
      draft.changeNicknameLoading = false;
      draft.changeNicknameError = action.error;
      break;

    case FOLLOW_REQUEST:
      draft.followingLoading = true;
      draft.followingError = null;
      draft.followingDone = false;
      break;

    case FOLLOW_SUCCESS:
      draft.followingLoading = false;
      draft.user.Followings.push({
        id: action.data.UserId
      });
      draft.followingDone = true;
      break;

    case FOLLOW_FAILURE:
      draft.followingLoading = false;
      draft.followingError = action.error;
      break;

    case UNFOLLOW_REQUEST:
      draft.unfollowingLoading = true;
      draft.unfollowingError = null;
      draft.unfollowingDone = false;
      break;

    case UNFOLLOW_SUCCESS:
      draft.unfollowingLoading = false;
      draft.user.Followings = draft.user.Followings.filter(v => v.id !== action.data.UserId);
      draft.unfollowingDone = true;
      break;

    case UNFOLLOW_FAILURE:
      draft.unfollowingLoading = false;
      draft.unfollowingError = action.error;
      break;

    case LOAD_FOLLOWERS_REQUEST:
      draft.loadFollowersLoading = true;
      draft.loadFollowersError = null;
      draft.loadFollowersDone = false;
      break;

    case LOAD_FOLLOWERS_SUCCESS:
      draft.user.Followers = action.data;
      draft.loadFollowersLoading = false;
      draft.loadFollowersDone = true;
      break;

    case LOAD_FOLLOWERS_FAILURE:
      draft.loadFollowersLoading = false;
      draft.loadFollowersError = action.error;
      break;

    case LOAD_FOLLOWINGS_REQUEST:
      draft.loadFollowingsLoading = true;
      draft.loadFollowingsError = null;
      draft.loadFollowingsDone = false;
      break;

    case LOAD_FOLLOWINGS_SUCCESS:
      draft.user.Followings = action.data;
      draft.loadFollowingsLoading = false;
      draft.loadFollowingsDone = true;
      break;

    case LOAD_FOLLOWINGS_FAILURE:
      draft.loadFollowingsLoading = false;
      draft.loadFollowingsError = action.error;
      break;

    case REMOVE_FOLLOWER_REQUEST:
      draft.removeFollowerLoading = true;
      draft.removeFollowerError = null;
      draft.removeFollowerDone = false;
      break;

    case REMOVE_FOLLOWER_SUCCESS:
      draft.user.Followers = draft.user.Followers.filter(v => v.id !== action.data.UserId);
      draft.removeFollowerLoading = false;
      draft.removeFollowerDone = true;
      break;

    case REMOVE_FOLLOWER_FAILURE:
      draft.removeFollowerLoading = false;
      draft.removeFollowerError = action.error;
      break;

    case _post__WEBPACK_IMPORTED_MODULE_1__/* .ADD_POST_TO_ME */ .x6:
      draft.user.Posts.unshift({
        id: action.data
      });
      break;

    case _post__WEBPACK_IMPORTED_MODULE_1__/* .REMOVE_POST_OF_ME */ .KK:
      draft.user.Posts = draft.user.Posts.filter(v => v.id !== action.data);
      break;

    default:
      break;
  }
}));

/***/ }),

/***/ 7675:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ store_configureStore)
});

// EXTERNAL MODULE: external "redux"
var external_redux_ = __webpack_require__(6695);
// EXTERNAL MODULE: external "next-redux-wrapper"
var external_next_redux_wrapper_ = __webpack_require__(5648);
// EXTERNAL MODULE: external "redux-devtools-extension"
var external_redux_devtools_extension_ = __webpack_require__(173);
// EXTERNAL MODULE: external "redux-saga"
var external_redux_saga_ = __webpack_require__(6537);
var external_redux_saga_default = /*#__PURE__*/__webpack_require__.n(external_redux_saga_);
// EXTERNAL MODULE: ./reducers/user.js
var user = __webpack_require__(8176);
// EXTERNAL MODULE: ./reducers/post.js
var post = __webpack_require__(3075);
;// CONCATENATED MODULE: ./reducers/index.js





const rootReducer = (state, action) => {
  switch (action.type) {
    case external_next_redux_wrapper_.HYDRATE:
      {
        return action.payload;
      }

    default:
      {
        const combinedReducer = (0,external_redux_.combineReducers)({
          user: user/* default */.ZP,
          post: post/* default */.Co
        });
        return combinedReducer(state, action);
      }
  }
};

/* harmony default export */ const reducers = (rootReducer);
// EXTERNAL MODULE: external "redux-saga/effects"
var effects_ = __webpack_require__(6477);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__(2167);
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);
;// CONCATENATED MODULE: ./sagas/user.js




function loadUserAPI(data) {
  return external_axios_default().get(`/user/${data}`);
}

function* loadUser(action) {
  try {
    const result = yield (0,effects_.call)(loadUserAPI, action.data);
    yield (0,effects_.put)({
      type: user/* LOAD_USER_SUCCESS */.DU,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: user/* LOAD_USER_FAILURE */.Ls,
      error: err.response.data
    });
  }
} // loadMyInfo


function loadMyInfoAPI() {
  return external_axios_default().get('/user');
}

function* loadMyInfo(action) {
  try {
    const result = yield (0,effects_.call)(loadMyInfoAPI, action.data);
    yield (0,effects_.put)({
      type: user/* LOAD_MY_INFO_SUCCESS */.LJ,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: user/* LOAD_MY_INFO_FAILURE */.Cq,
      error: err.response.data
    });
  }
} // loadMyInfo
// login


function logInAPI(data) {
  return external_axios_default().post('/user/login', data);
}

function* logIn(action) {
  try {
    const result = yield (0,effects_.call)(logInAPI, action.data);
    yield (0,effects_.put)({
      type: user/* LOG_IN_SUCCESS */.RZ,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: user/* LOG_IN_FAILURE */.yK,
      error: err.response.data
    });
  }
} // login
// logout


function logOutAPI() {
  return external_axios_default().post('/user/logout');
}

function* logOut() {
  try {
    yield (0,effects_.call)(logOutAPI);
    yield (0,effects_.put)({
      type: user/* LOG_OUT_SUCCESS */.kV
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: user/* LOG_OUT_FAILURE */.mD,
      error: err.response.data
    });
  }
} // logout
// signUp


function signUpAPI(data) {
  return external_axios_default().post('/user', data);
}

function* signUp(action) {
  try {
    const result = yield (0,effects_.call)(signUpAPI, action.data);
    yield (0,effects_.put)({
      type: user/* SIGN_UP_SUCCESS */.I,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: user/* SIGN_UP_FAILURE */.bP,
      error: err.response.data
    });
  }
} // signUp
// following


function followingAPI(data) {
  return external_axios_default().patch(`/user/${data}/follow`);
}

function* following(action) {
  try {
    const result = yield (0,effects_.call)(followingAPI, action.data);
    yield (0,effects_.put)({
      type: user/* FOLLOW_SUCCESS */.mv,
      data: result.data
    });
  } catch (e) {
    console.error(e);
    yield (0,effects_.put)({
      type: user/* FOLLOW_FAILURE */.DG,
      error: e.response.data
    });
  }
} // following
// unfollowing


function unfollowingAPI(data) {
  return external_axios_default()["delete"](`/user/${data}/follow`);
}

function* unfollowing(action) {
  try {
    const result = yield (0,effects_.call)(unfollowingAPI, action.data);
    yield (0,effects_.put)({
      type: user/* UNFOLLOW_SUCCESS */.D7,
      data: result.data
    });
  } catch (e) {
    console.error(e);
    yield (0,effects_.put)({
      type: user/* UNFOLLOW_FAILURE */.OR,
      error: e.response.data
    });
  }
} // unfollowing
// changeNickname


function changeNicknameAPI(data) {
  return external_axios_default().patch('/user/nickname', {
    nickname: data
  });
}

function* changeNickname(action) {
  try {
    const result = yield (0,effects_.call)(changeNicknameAPI, action.data);
    yield (0,effects_.put)({
      type: user/* CHANGE_NICKNAME_SUCCESS */.dr,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: user/* CHANGE_NICKNAME_FAILURE */.PG,
      error: err.response.data
    });
  }
} // changeNickname
// loadFollowings


function loadFollowingsAPI(data) {
  return external_axios_default().get('/user/followings', data);
}

function* loadFollowings(action) {
  try {
    const result = yield (0,effects_.call)(loadFollowingsAPI, action.data);
    yield (0,effects_.put)({
      type: user/* LOAD_FOLLOWINGS_SUCCESS */.IQ,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: user/* LOAD_FOLLOWINGS_FAILURE */.qt,
      error: err.response.data
    });
  }
} // loadFollowings
// loadFollowers


function loadFollowersAPI(data) {
  return external_axios_default().get('/user/followers', data);
}

function* loadFollowers(action) {
  try {
    const result = yield (0,effects_.call)(loadFollowersAPI, action.data);
    yield (0,effects_.put)({
      type: user/* LOAD_FOLLOWERS_SUCCESS */.Y6,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: user/* LOAD_FOLLOWERS_FAILURE */.vT,
      error: err.response.data
    });
  }
} // loadFollowers
// removeFollower


function removeFollowerAPI(data) {
  return external_axios_default()["delete"](`/user/follower/${data}`);
}

function* removeFollower(action) {
  try {
    const result = yield (0,effects_.call)(removeFollowerAPI, action.data);
    yield (0,effects_.put)({
      type: user/* REMOVE_FOLLOWER_SUCCESS */.pe,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: user/* REMOVE_FOLLOWER_FAILURE */.IK,
      error: err.response.data
    });
  }
} // removeFollower


function* watchLoadUser() {
  yield (0,effects_.takeLatest)(user/* LOAD_USER_REQUEST */.dQ, loadUser);
}

function* watchLoadMyInfo() {
  yield (0,effects_.takeLatest)(user/* LOAD_MY_INFO_REQUEST */.qq, loadMyInfo);
}

function* watchLogIn() {
  yield (0,effects_.takeLatest)(user/* LOG_IN_REQUEST */.uF, logIn);
}

function* watchLogOut() {
  yield (0,effects_.takeLatest)(user/* LOG_OUT_REQUEST */.Oy, logOut);
}

function* watchSignUp() {
  yield (0,effects_.takeLatest)(user/* SIGN_UP_REQUEST */.pK, signUp);
}

function* watchFollowing() {
  yield (0,effects_.takeLatest)(user/* FOLLOW_REQUEST */.U_, following);
}

function* watchUnfollowing() {
  yield (0,effects_.takeLatest)(user/* UNFOLLOW_REQUEST */.Bk, unfollowing);
}

function* watchChangeNickname() {
  yield (0,effects_.takeLatest)(user/* CHANGE_NICKNAME_REQUEST */.o, changeNickname);
}

function* watchLoadFollowers() {
  yield (0,effects_.takeLatest)(user/* LOAD_FOLLOWERS_REQUEST */.G$, loadFollowers);
}

function* watchLoadFollowings() {
  yield (0,effects_.takeLatest)(user/* LOAD_FOLLOWINGS_REQUEST */.Xd, loadFollowings);
}

function* watchRemoveFollower() {
  yield (0,effects_.takeLatest)(user/* REMOVE_FOLLOWER_REQUEST */.IB, removeFollower);
}

function* userSaga() {
  yield (0,effects_.all)([(0,effects_.fork)(watchLoadUser), (0,effects_.fork)(watchLoadMyInfo), (0,effects_.fork)(watchLogIn), (0,effects_.fork)(watchLogOut), (0,effects_.fork)(watchSignUp), (0,effects_.fork)(watchFollowing), (0,effects_.fork)(watchUnfollowing), (0,effects_.fork)(watchChangeNickname), (0,effects_.fork)(watchLoadFollowers), (0,effects_.fork)(watchLoadFollowings), (0,effects_.fork)(watchRemoveFollower)]);
}
;// CONCATENATED MODULE: ./sagas/post.js




function loadPostsAPI(lastId) {
  return external_axios_default().get(`/posts?lastId=${lastId || 0}`);
} // loadPosts


function* loadPosts(action) {
  try {
    const result = yield (0,effects_.call)(loadPostsAPI, action.lastId);
    yield (0,effects_.put)({
      type: post/* LOAD_POSTS_SUCCESS */.ZP,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: post/* LOAD_POSTS_FAILURE */.T5,
      error: err.response.data
    });
  }
} // loadPost


function loadPostAPI(data) {
  return external_axios_default().get(`/post/${data}`);
}

function* loadPost(action) {
  try {
    const result = yield (0,effects_.call)(loadPostAPI, action.data);
    yield (0,effects_.put)({
      type: post/* LOAD_POST_SUCCESS */.YQ,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: post/* LOAD_POST_FAILURE */.rl,
      data: err.response.data
    });
  }
} // loadPost


function loadUserPostsAPI(data, lastId) {
  return external_axios_default().get(`/user/${data}/posts?lastId=${lastId || 0}`);
}

function* loadUserPosts(action) {
  try {
    const result = yield (0,effects_.call)(loadUserPostsAPI, action.data, action.lastId);
    yield (0,effects_.put)({
      type: post/* LOAD_USER_POSTS_SUCCESS */.Ag,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: post/* LOAD_USER_POSTS_FAILURE */.N3,
      data: err.response.data
    });
  }
}

function loadHashtagPostsAPI(data, lastId) {
  return external_axios_default().get(`/hashtag/${encodeURIComponent(data)}?lastId=${lastId || 0}`);
}

function* loadHashtagPosts(action) {
  try {
    const result = yield (0,effects_.call)(loadHashtagPostsAPI, action.data, action.lastId);
    yield (0,effects_.put)({
      type: post/* LOAD_HASHTAG_POSTS_SUCCESS */.LF,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: post/* LOAD_HASHTAG_POSTS_FAILURE */.uk,
      data: err.response.data
    });
  }
}

function addPostAPI(data) {
  return external_axios_default().post('/post', data);
} // addPost


function* addPost(action) {
  try {
    const result = yield (0,effects_.call)(addPostAPI, action.data);
    yield (0,effects_.put)({
      type: post/* ADD_POST_SUCCESS */._s,
      data: result.data
    });
    yield (0,effects_.put)({
      type: post/* ADD_POST_TO_ME */.x6,
      data: result.data.id
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: post/* ADD_POST_FAILURE */.tP,
      error: err.response.data
    });
  }
} // addPost


function removePostAPI(data) {
  return external_axios_default()["delete"](`/post/${data}`);
} // removePost


function* removePost(action) {
  try {
    const result = yield (0,effects_.call)(removePostAPI, action.data);
    yield (0,effects_.put)({
      type: post/* REMOVE_POST_SUCCESS */.rK,
      data: result.data
    });
    yield (0,effects_.put)({
      type: post/* REMOVE_POST_OF_ME */.KK,
      data: action.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: post/* REMOVE_POST_FAILURE */.Ws,
      error: err.response.data
    });
  }
} // removePost


function addCommentAPI(data) {
  return external_axios_default().post(`/post/${data.postId}/comment`, data);
} // addComment


function* addComment(action) {
  try {
    const result = yield (0,effects_.call)(addCommentAPI, action.data);
    yield (0,effects_.put)({
      type: post/* ADD_COMMENT_SUCCESS */.nv,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: post/* ADD_COMMENT_FAILURE */.rX,
      error: err.response.data
    });
  }
} // addComment


function likePostAPI(data) {
  return external_axios_default().patch(`/post/${data}/like`);
} // likePost


function* likePost(action) {
  try {
    const result = yield (0,effects_.call)(likePostAPI, action.data);
    yield (0,effects_.put)({
      type: post/* LIKE_POST_SUCCESS */.Gz,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: post/* LIKE_POST_FAILURE */.cT,
      error: err.response.data
    });
  }
} // likePost


function unlikePostAPI(data) {
  return external_axios_default()["delete"](`/post/${data}/like`);
} // unlikePost


function* unlikePost(action) {
  try {
    const result = yield (0,effects_.call)(unlikePostAPI, action.data);
    yield (0,effects_.put)({
      type: post/* UNLIKE_POST_SUCCESS */.XD,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: post/* UNLIKE_POST_FAILURE */.gq,
      error: err.response.data
    });
  }
} // unlikePost


function uploadImagesAPI(data) {
  return external_axios_default().post('/post/images', data);
} // uploadImages


function* uploadImages(action) {
  try {
    const result = yield (0,effects_.call)(uploadImagesAPI, action.data);
    yield (0,effects_.put)({
      type: post/* UPLOAD_IMAGES_SUCCESS */.kv,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: post/* UPLOAD_IMAGES_FAILURE */.hX,
      error: err.response.data
    });
  }
} // uploadImages


function retweetAPI(data) {
  return external_axios_default().post(`/post/${data}/retweet`);
} // retweet


function* retweet(action) {
  try {
    const result = yield (0,effects_.call)(retweetAPI, action.data);
    yield (0,effects_.put)({
      type: post/* RETWEET_SUCCESS */.ZL,
      data: result.data
    });
  } catch (err) {
    console.error(err);
    yield (0,effects_.put)({
      type: post/* RETWEET_FAILURE */.FM,
      error: err.response.data
    });
  }
} // retweet


function* watchLoadPosts() {
  yield (0,effects_.throttle)(5000, post/* LOAD_POSTS_REQUEST */.aO, loadPosts);
}

function* watchLoadPost() {
  yield (0,effects_.takeLatest)(post/* LOAD_POST_REQUEST */.EG, loadPost);
}

function* watchUserLoadPosts() {
  yield (0,effects_.throttle)(5000, post/* LOAD_USER_POSTS_REQUEST */.x5, loadUserPosts);
}

function* watchHashtagLoadPosts() {
  yield (0,effects_.throttle)(5000, post/* LOAD_HASHTAG_POSTS_REQUEST */.az, loadHashtagPosts);
}

function* watchAddPost() {
  yield (0,effects_.takeLatest)(post/* ADD_POST_REQUEST */.z9, addPost);
}

function* watchAddComment() {
  yield (0,effects_.takeLatest)(post/* ADD_COMMENT_REQUEST */.Ot, addComment);
}

function* watchRemovePost() {
  yield (0,effects_.takeLatest)(post/* REMOVE_POST_REQUEST */.HU, removePost);
}

function* watchLikePost() {
  yield (0,effects_.takeLatest)(post/* LIKE_POST_REQUEST */.xD, likePost);
}

function* watchUnLikePost() {
  yield (0,effects_.takeLatest)(post/* UNLIKE_POST_REQUEST */.VT, unlikePost);
}

function* watchUploadImages() {
  yield (0,effects_.takeLatest)(post/* UPLOAD_IMAGES_REQUEST */.QA, uploadImages);
}

function* watchRetweet() {
  yield (0,effects_.takeLatest)(post/* RETWEET_REQUEST */.a0, retweet);
}

function* postSaga() {
  yield (0,effects_.all)([(0,effects_.fork)(watchLoadPosts), (0,effects_.fork)(watchLoadPost), (0,effects_.fork)(watchUserLoadPosts), (0,effects_.fork)(watchHashtagLoadPosts), (0,effects_.fork)(watchAddPost), (0,effects_.fork)(watchRemovePost), (0,effects_.fork)(watchAddComment), (0,effects_.fork)(watchLikePost), (0,effects_.fork)(watchUnLikePost), (0,effects_.fork)(watchUploadImages), (0,effects_.fork)(watchRetweet)]);
}
;// CONCATENATED MODULE: ./sagas/index.js




(external_axios_default()).defaults.baseURL = 'http://localhost:3065';
(external_axios_default()).defaults.withCredentials = true;
function* rootSaga() {
  yield (0,effects_.all)([(0,effects_.fork)(userSaga), (0,effects_.fork)(postSaga)]);
}
;// CONCATENATED MODULE: ./store/configureStore.js







const loggerMiddleware = () => next => action => next(action);

const configureStore = () => {
  const sagaMiddleware = external_redux_saga_default()();
  const middlewares = [sagaMiddleware, loggerMiddleware];
  const enhancer =  true ? (0,external_redux_.compose)((0,external_redux_.applyMiddleware)(...middlewares)) : 0;
  const store = (0,external_redux_.createStore)(reducers, enhancer);
  store.sagaTask = sagaMiddleware.run(rootSaga);
  return store;
};

const wrapper = (0,external_next_redux_wrapper_.createWrapper)(configureStore, {
  debug: false
});
/* harmony default export */ const store_configureStore = (wrapper);

/***/ })

};
;