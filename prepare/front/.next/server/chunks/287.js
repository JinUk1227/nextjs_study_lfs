"use strict";
exports.id = 287;
exports.ids = [287];
exports.modules = {

/***/ 1287:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ components_AppLayout)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: external "antd"
var external_antd_ = __webpack_require__(5725);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
var router_default = /*#__PURE__*/__webpack_require__.n(router_);
// EXTERNAL MODULE: ./hooks/useInput.js
var useInput = __webpack_require__(3551);
// EXTERNAL MODULE: ./reducers/user.js
var reducers_user = __webpack_require__(8176);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./components/LoginForm.js









const LoginForm = function () {
  const [email, onChangeEmail] = (0,useInput/* default */.Z)('');
  const [password, onChangePassword] = (0,useInput/* default */.Z)('');
  const dispatch = (0,external_react_redux_.useDispatch)();
  const {
    logInLoading,
    logInError
  } = (0,external_react_redux_.useSelector)(state => state.user);
  (0,external_react_.useEffect)(() => {
    if (logInError) {
      alert(logInError);
    }
  }, [logInError]);
  const onSubmitForm = (0,external_react_.useCallback)(() => {
    dispatch({
      type: reducers_user/* LOG_IN_REQUEST */.uF,
      data: {
        email,
        password
      }
    });
  }, [email, password]);
  return (
    /*#__PURE__*/
    // eslint-disable-next-line react/jsx-filename-extension
    (0,jsx_runtime_.jsxs)(external_antd_.Form, {
      onFinish: onSubmitForm,
      style: {
        padding: '10px'
      },
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
          htmlFor: "user-email",
          children: "\uC774\uBA54\uC77C"
        }), /*#__PURE__*/jsx_runtime_.jsx("br", {}), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Input, {
          name: "user-email",
          value: email,
          onChange: onChangeEmail,
          required: true
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
          htmlFor: "user-password",
          children: "\uBE44\uBC00\uBC88\uD638"
        }), /*#__PURE__*/jsx_runtime_.jsx("br", {}), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Input, {
          name: "user-password",
          value: password,
          onChange: onChangePassword,
          type: "password",
          required: true
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        style: {
          marginTop: '10px'
        },
        children: [/*#__PURE__*/jsx_runtime_.jsx(external_antd_.Button, {
          type: "primary",
          htmlType: "submit",
          loading: logInLoading,
          children: "\uB85C\uADF8\uC778"
        }), /*#__PURE__*/jsx_runtime_.jsx(next_link["default"], {
          href: "/signup",
          children: /*#__PURE__*/jsx_runtime_.jsx("a", {
            children: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Button, {
              children: "\uD68C\uC6D0\uAC00\uC785"
            })
          })
        })]
      })]
    })
  );
};

/* harmony default export */ const components_LoginForm = (LoginForm);
;// CONCATENATED MODULE: ./components/UserProfile.js








const UserProfile = function () {
  const dispatch = (0,external_react_redux_.useDispatch)();
  const {
    user,
    logOutLoading
  } = (0,external_react_redux_.useSelector)(state => state.user);
  const onLogout = (0,external_react_.useCallback)(() => {
    dispatch(reducers_user/* logoutRequestAction */.vR);
  }, []);
  return (
    /*#__PURE__*/
    // eslint-disable-next-line react/jsx-filename-extension
    (0,jsx_runtime_.jsxs)(external_antd_.Card, {
      actions: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        children: /*#__PURE__*/jsx_runtime_.jsx(next_link["default"], {
          href: `/user/${user.id}`,
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
            children: ["\uC9F9\uC9F9", /*#__PURE__*/jsx_runtime_.jsx("br", {}), user.Posts.length]
          })
        })
      }, "twit"), /*#__PURE__*/jsx_runtime_.jsx("div", {
        children: /*#__PURE__*/jsx_runtime_.jsx(next_link["default"], {
          href: "/profile",
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
            children: ["\uD314\uB85C\uC789", /*#__PURE__*/jsx_runtime_.jsx("br", {}), user.Followings.length]
          })
        })
      }, "following"), /*#__PURE__*/jsx_runtime_.jsx("div", {
        children: /*#__PURE__*/jsx_runtime_.jsx(next_link["default"], {
          href: "/profile",
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
            children: ["\uD314\uB85C\uC6CC", /*#__PURE__*/jsx_runtime_.jsx("br", {}), user.Followers.length]
          })
        })
      }, "follower")],
      children: [/*#__PURE__*/jsx_runtime_.jsx(external_antd_.Card.Meta, {
        avatar: /*#__PURE__*/jsx_runtime_.jsx(next_link["default"], {
          href: `/user/${user.id}`,
          children: /*#__PURE__*/jsx_runtime_.jsx("a", {
            children: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Avatar, {
              children: user.nickname[0]
            })
          })
        }),
        title: user.nickname
      }), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Button, {
        onClick: onLogout,
        loading: logOutLoading,
        children: "\uB85C\uADF8\uC544\uC6C3"
      })]
    })
  );
};

/* harmony default export */ const components_UserProfile = (UserProfile);
;// CONCATENATED MODULE: ./components/AppLayout.js











const Global = (0,external_styled_components_.createGlobalStyle)([".ant-row{margin-right:0 !important;margin-left:0 !important;}.ant-col:first-child{padding-left:0 !important;}.ant-col:last-child{padding-right:0 !important;}"]);

const AppLayout = function ({
  children
}) {
  const {
    user
  } = (0,external_react_redux_.useSelector)(state => state.user);
  const [searchInput, onChangeSearchInput] = (0,useInput/* default */.Z)('');
  const onSearch = (0,external_react_.useCallback)(() => {
    router_default().push(`/hashtag/${searchInput}`);
  }, [searchInput]);
  return (
    /*#__PURE__*/
    // eslint-disable-next-line react/jsx-filename-extension
    (0,jsx_runtime_.jsxs)("div", {
      children: [/*#__PURE__*/jsx_runtime_.jsx(Global, {}), /*#__PURE__*/(0,jsx_runtime_.jsxs)(external_antd_.Menu, {
        mode: "horizontal",
        children: [/*#__PURE__*/jsx_runtime_.jsx(external_antd_.Menu.Item, {
          children: /*#__PURE__*/jsx_runtime_.jsx(next_link["default"], {
            href: "/",
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              children: "\uB178\uB4DC\uBC84\uB4DC"
            })
          })
        }, "home"), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Menu.Item, {
          children: /*#__PURE__*/jsx_runtime_.jsx(next_link["default"], {
            href: "/profile",
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              children: "\uD504\uB85C\uD544"
            })
          })
        }, "profile"), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Menu.Item, {
          children: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Input.Search, {
            enterButton: true,
            style: {
              verticalAlign: 'middle'
            },
            value: searchInput,
            onChange: onChangeSearchInput,
            onSearch: onSearch
          })
        }, "mail")]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(external_antd_.Row, {
        gutter: 8,
        children: [/*#__PURE__*/jsx_runtime_.jsx(external_antd_.Col, {
          xs: 24,
          md: 6,
          children: user ? /*#__PURE__*/jsx_runtime_.jsx(components_UserProfile, {}) : /*#__PURE__*/jsx_runtime_.jsx(components_LoginForm, {})
        }), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Col, {
          xs: 24,
          md: 12,
          children: children
        }), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Col, {
          xs: 24,
          md: 6,
          children: /*#__PURE__*/jsx_runtime_.jsx("a", {
            href: "https://abiding-methane-eba.notion.site/1fb75c620b1e41e8b736a17ea24ad4b2",
            target: "_blank",
            rel: "noreferrer noopener",
            children: "Made by Jindol"
          })
        })]
      })]
    })
  );
};

/* harmony default export */ const components_AppLayout = (AppLayout);

/***/ }),

/***/ 3551:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((initValue = null) => {
  const {
    0: value,
    1: setter
  } = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(initValue);
  const handler = (0,react__WEBPACK_IMPORTED_MODULE_0__.useCallback)(e => {
    setter(e.target.value);
  }, []);
  return [value, handler, setter];
});

/***/ })

};
;