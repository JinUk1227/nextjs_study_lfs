"use strict";
exports.id = 160;
exports.ids = [160];
exports.modules = {

/***/ 9160:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ components_PostCard)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: external "antd"
var external_antd_ = __webpack_require__(5725);
// EXTERNAL MODULE: external "@ant-design/icons"
var icons_ = __webpack_require__(7066);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
// EXTERNAL MODULE: external "dayjs"
var external_dayjs_ = __webpack_require__(1635);
var external_dayjs_default = /*#__PURE__*/__webpack_require__.n(external_dayjs_);
// EXTERNAL MODULE: ./reducers/post.js
var reducers_post = __webpack_require__(3075);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./components/CommentForm.js







const CommentForm = function ({
  post
}) {
  const id = (0,external_react_redux_.useSelector)(state => {
    var _state$user$user;

    return (_state$user$user = state.user.user) === null || _state$user$user === void 0 ? void 0 : _state$user$user.id;
  });
  const {
    addCommentDone,
    addCommentLoading
  } = (0,external_react_redux_.useSelector)(state => state.post);
  const {
    0: commentText,
    1: setCommentText
  } = (0,external_react_.useState)('');
  const dispatch = (0,external_react_redux_.useDispatch)();
  (0,external_react_.useEffect)(() => {
    if (addCommentDone) {
      setCommentText('');
    }
  }, [addCommentDone]);
  const onSubmitComment = (0,external_react_.useCallback)(() => {
    dispatch({
      type: reducers_post/* ADD_COMMENT_REQUEST */.Ot,
      data: {
        content: commentText,
        postId: post.id,
        userId: id
      }
    });
  }, [commentText, id]);
  const onChangeCommentText = (0,external_react_.useCallback)(e => {
    setCommentText(e.target.value);
  }, []);
  return (
    /*#__PURE__*/
    // eslint-disable-next-line react/jsx-filename-extension
    jsx_runtime_.jsx(external_antd_.Form, {
      onFinish: onSubmitComment,
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(external_antd_.Form.Item, {
        style: {
          position: 'relative',
          margin: 0
        },
        children: [/*#__PURE__*/jsx_runtime_.jsx(external_antd_.Input.TextArea, {
          rows: 4,
          value: commentText,
          onChange: onChangeCommentText
        }), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Button, {
          style: {
            position: 'absolute',
            right: 0,
            bottom: -40,
            zIndex: 1
          },
          type: "primary",
          htmlType: "submit",
          loading: addCommentLoading,
          children: "\uC090\uC57D"
        })]
      })
    })
  );
};

/* harmony default export */ const components_CommentForm = (CommentForm);
;// CONCATENATED MODULE: ./components/PostCardContent.js




const PostCardContent = function ({
  postData
}) {
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    children: postData.split(/(#[^\s#]+)/g).map((v, i) => {
      if (v.match(/(#[^\s#]+)/)) {
        return /*#__PURE__*/jsx_runtime_.jsx(next_link["default"], {
          href: `/hashtag/${v.slice(1)}`,
          children: /*#__PURE__*/jsx_runtime_.jsx("a", {
            children: v
          })
        }, i);
      }

      return v;
    })
  });
};

/* harmony default export */ const components_PostCardContent = (PostCardContent);
// EXTERNAL MODULE: external "react-slick"
var external_react_slick_ = __webpack_require__(8096);
var external_react_slick_default = /*#__PURE__*/__webpack_require__.n(external_react_slick_);
;// CONCATENATED MODULE: ./components/ImagesZoom/styles.js


const Global = (0,external_styled_components_.createGlobalStyle)([".slick-slide{display:inline-block;}.ant-card-cover{transform:none !important;}"]);
const Overlay = external_styled_components_default().div.withConfig({
  displayName: "styles__Overlay",
  componentId: "sc-r2o2ee-0"
})(["position:fixed;z-index:5000;top:0;left:0;right:0;bottom:0;"]);
const Header = external_styled_components_default().header.withConfig({
  displayName: "styles__Header",
  componentId: "sc-r2o2ee-1"
})(["height:44px;background:white;position:relative;padding:0;text-align:center;& h1{margin:0;font-size:17px;color:#333;line-height:44px;}"]);
const SlickWrapper = external_styled_components_default().div.withConfig({
  displayName: "styles__SlickWrapper",
  componentId: "sc-r2o2ee-2"
})(["height:calc(100% - 44px);background:#090909;"]);
const CloseBtn = external_styled_components_default()(icons_.CloseOutlined).withConfig({
  displayName: "styles__CloseBtn",
  componentId: "sc-r2o2ee-3"
})(["position:absolute;right:0;top:0;padding:15px;line-height:14px;cursor:pointer;"]);
const Indicator = external_styled_components_default().div.withConfig({
  displayName: "styles__Indicator",
  componentId: "sc-r2o2ee-4"
})(["text-align:center;& > div{width:75px;height:30px;line-height:30px;border-radius:15px;background:#313131;display:inline-block;text-align:center;color:white;font-size:15px;}"]);
const ImgWrapper = external_styled_components_default().div.withConfig({
  displayName: "styles__ImgWrapper",
  componentId: "sc-r2o2ee-5"
})(["padding:32px;text-align:center;& img{margin:0 auto;max-height:750px;}"]);
;// CONCATENATED MODULE: ./components/ImagesZoom/index.js






const ImagesZoom = function ({
  images,
  onClose
}) {
  const {
    0: currentSlide,
    1: setCurrentSlide
  } = (0,external_react_.useState)(0);
  return (
    /*#__PURE__*/
    // eslint-disable-next-line react/jsx-filename-extension
    (0,jsx_runtime_.jsxs)(Overlay, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(Global, {}), /*#__PURE__*/(0,jsx_runtime_.jsxs)(Header, {
        children: [/*#__PURE__*/jsx_runtime_.jsx("h1", {
          children: "\uC0C1\uC138 \uC774\uBBF8\uC9C0"
        }), /*#__PURE__*/jsx_runtime_.jsx(CloseBtn, {
          onClick: onClose
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx(SlickWrapper, {
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          children: [/*#__PURE__*/jsx_runtime_.jsx((external_react_slick_default()), {
            initialSlide: 0,
            beforeChange: (slide, newSlide) => setCurrentSlide(newSlide),
            infinite: true,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            children: images.map(v => /*#__PURE__*/jsx_runtime_.jsx(ImgWrapper, {
              children: /*#__PURE__*/jsx_runtime_.jsx("img", {
                src: `http://localhost:3065/${v.src}`,
                alt: `http://localhost:3065/${v.src}`
              })
            }, v.src))
          }), /*#__PURE__*/jsx_runtime_.jsx(Indicator, {
            children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
              children: [currentSlide + 1, ' ', "/", images.length]
            })
          })]
        })
      })]
    })
  );
};

/* harmony default export */ const components_ImagesZoom = (ImagesZoom);
;// CONCATENATED MODULE: ./components/PostImages.js







const PostImages = function ({
  images
}) {
  const {
    0: showImagesZoom,
    1: setShowImagesZoom
  } = (0,external_react_.useState)(false);
  const onZoom = (0,external_react_.useCallback)(() => {
    setShowImagesZoom(true);
  }, []);
  const onClose = (0,external_react_.useCallback)(() => {
    setShowImagesZoom(false);
  }, []);

  if (images.length === 1) {
    return /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
      children: [/*#__PURE__*/jsx_runtime_.jsx("img", {
        role: "presentation",
        src: `http://localhost:3065/${images[0].src}`,
        alt: images[0].src,
        onClick: onZoom
      }), showImagesZoom && /*#__PURE__*/jsx_runtime_.jsx(components_ImagesZoom, {
        images: images,
        onClose: onClose
      })]
    });
  }

  if (images.length === 2) {
    return /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        children: [/*#__PURE__*/jsx_runtime_.jsx("img", {
          role: "presentation",
          src: `http://localhost:3065/${images[0].src}`,
          alt: `http://localhost:3065/${images[0].src}`,
          width: "50%",
          onClick: onZoom
        }), /*#__PURE__*/jsx_runtime_.jsx("img", {
          role: "presentation",
          src: `http://localhost:3065/${images[1].src}`,
          alt: `http://localhost:3065/${images[1].src}`,
          width: "50%",
          onClick: onZoom
        })]
      }), showImagesZoom && /*#__PURE__*/jsx_runtime_.jsx(components_ImagesZoom, {
        images: images,
        onClose: onClose
      })]
    });
  }

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      children: [/*#__PURE__*/jsx_runtime_.jsx("img", {
        role: "presentation",
        src: `http://localhost:3065/${images[0].src}`,
        alt: `http://localhost:3065/${images[0].src}`,
        width: "50%",
        onClick: onZoom
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        role: "presentation",
        style: {
          display: 'inline-block',
          width: '50%',
          textAlign: 'center',
          verticalAlign: 'middle'
        },
        onClick: onZoom,
        children: [/*#__PURE__*/jsx_runtime_.jsx(icons_.PlusOutlined, {}), /*#__PURE__*/jsx_runtime_.jsx("br", {}), images.length - 1, "\uAC1C\uC758 \uC0AC\uC9C4 \uB354\uBCF4\uAE30"]
      })]
    }), showImagesZoom && /*#__PURE__*/jsx_runtime_.jsx(components_ImagesZoom, {
      images: images,
      onClose: onClose
    })]
  });
};

/* harmony default export */ const components_PostImages = (PostImages);
// EXTERNAL MODULE: ./reducers/user.js
var reducers_user = __webpack_require__(8176);
;// CONCATENATED MODULE: ./components/FollowButton.js






const FollowButton = function ({
  post
}) {
  const dispatch = (0,external_react_redux_.useDispatch)();
  const {
    user,
    followingLoading,
    unfollowingLoading
  } = (0,external_react_redux_.useSelector)(state => state.user);
  const isFollowing = user === null || user === void 0 ? void 0 : user.Followings.find(v => v.id === post.User.id);
  const onCLickButton = (0,external_react_.useCallback)(() => {
    if (isFollowing) {
      dispatch({
        type: reducers_user/* UNFOLLOW_REQUEST */.Bk,
        data: post.User.id
      });
    } else {
      dispatch({
        type: reducers_user/* FOLLOW_REQUEST */.U_,
        data: post.User.id
      });
    }
  }, [isFollowing]);

  if (post.User.id === user.id) {
    return null;
  }

  return /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Button, {
    loading: followingLoading || unfollowingLoading,
    onClick: onCLickButton,
    children: isFollowing ? '언팔로우' : '팔로우'
  });
};

/* harmony default export */ const components_FollowButton = (FollowButton);
;// CONCATENATED MODULE: ./components/PostCard.js















external_dayjs_default().locale('ko');
const CardWrapper = external_styled_components_default().div.withConfig({
  displayName: "PostCard__CardWrapper",
  componentId: "sc-10lmnn1-0"
})(["margin-bottom:20px;"]);
const RetweetCard = external_styled_components_default()(external_antd_.Card).withConfig({
  displayName: "PostCard__RetweetCard",
  componentId: "sc-10lmnn1-1"
})(["margin-top:20px;"]);

const PostCard = function ({
  post
}) {
  const {
    0: commentFormOpened,
    1: setCommentFormOpened
  } = (0,external_react_.useState)(false);
  const {
    removePostLoading
  } = (0,external_react_redux_.useSelector)(state => state.post);
  const id = (0,external_react_redux_.useSelector)(state => {
    var _state$user$user;

    return (_state$user$user = state.user.user) === null || _state$user$user === void 0 ? void 0 : _state$user$user.id;
  });
  const dispatch = (0,external_react_redux_.useDispatch)();
  const onLike = (0,external_react_.useCallback)(() => {
    if (!id) {
      return alert('로그인이 필요합니다');
    }

    return dispatch({
      type: reducers_post/* LIKE_POST_REQUEST */.xD,
      data: post.id
    });
  }, [id]);
  const onUnLike = (0,external_react_.useCallback)(() => {
    if (!id) {
      return alert('로그인이 필요합니다');
    }

    return dispatch({
      type: reducers_post/* UNLIKE_POST_REQUEST */.VT,
      data: post.id
    });
  }, [id]);
  const onToggleComment = (0,external_react_.useCallback)(() => {
    setCommentFormOpened(prev => !prev);
  }, []);
  const onRemovePost = (0,external_react_.useCallback)(() => {
    if (!id) {
      return alert('로그인이 필요합니다');
    }

    return dispatch({
      type: reducers_post/* REMOVE_POST_REQUEST */.HU,
      data: post.id
    });
  }, [post.id, id]);
  const onRetweet = (0,external_react_.useCallback)(() => {
    if (!id) {
      return alert('로그인이 필요합니다.');
    }

    return dispatch({
      type: reducers_post/* RETWEET_REQUEST */.a0,
      data: post.id
    });
  }, [id]);
  const liked = post.Likers.find(v => v.id === id);
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(CardWrapper, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(external_antd_.Card, {
      cover: post.Images[0] && /*#__PURE__*/jsx_runtime_.jsx(components_PostImages, {
        images: post.Images
      }),
      actions: [/*#__PURE__*/jsx_runtime_.jsx(icons_.RetweetOutlined, {
        onClick: onRetweet
      }, "retweet"), liked ? /*#__PURE__*/jsx_runtime_.jsx(icons_.HeartTwoTone, {
        twoToneColor: "#eb2f96",
        onClick: onUnLike
      }, "heart") : /*#__PURE__*/jsx_runtime_.jsx(icons_.HeartOutlined, {
        onClick: onLike
      }, "heart"), /*#__PURE__*/jsx_runtime_.jsx(icons_.MessageOutlined, {
        onClick: onToggleComment
      }, "message"), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Popover, {
        content: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Button.Group, {
          children: id && post.User.id === id ? /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
            children: [/*#__PURE__*/jsx_runtime_.jsx(external_antd_.Button, {
              children: "\uC218\uC815"
            }), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Button, {
              type: "danger",
              loading: removePostLoading,
              onClick: onRemovePost,
              children: "\uC0AD\uC81C"
            })]
          }) : /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Button, {
            children: "\uC2E0\uACE0"
          })
        }),
        children: /*#__PURE__*/jsx_runtime_.jsx(icons_.EllipsisOutlined, {})
      }, "ellipsis")],
      extra: id && /*#__PURE__*/jsx_runtime_.jsx(components_FollowButton, {
        post: post
      }),
      children: post.RetweetId && post.Retweet ? /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
          style: {
            float: 'right'
          },
          children: external_dayjs_default()(post.createdAt).format('YYYY.MM.DD')
        }), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Card.Meta, {
          avatar: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Avatar, {
            children: post.User.nickname[0]
          }),
          title: post.User.nickname
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)(RetweetCard, {
          cover: post.Retweet.Images[0] && /*#__PURE__*/jsx_runtime_.jsx(components_PostImages, {
            images: post.Retweet.Images
          }),
          children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
            style: {
              float: 'right'
            },
            children: external_dayjs_default()(post.Retweet.createdAt).format('YYYY.MM.DD')
          }), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Card.Meta, {
            avatar: /*#__PURE__*/jsx_runtime_.jsx(next_link["default"], {
              href: `/user/${post.Retweet.User.id}`,
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                children: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Avatar, {
                  children: post.Retweet.User.nickname[0]
                })
              })
            }),
            title: post.Retweet.User.nickname,
            description: /*#__PURE__*/jsx_runtime_.jsx(components_PostCardContent, {
              postData: post.Retweet.content
            })
          })]
        })]
      }) : /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
        children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
          style: {
            float: 'right'
          },
          children: external_dayjs_default()(post.createdAt).format('YYYY.MM.DD')
        }), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Card.Meta, {
          avatar: /*#__PURE__*/jsx_runtime_.jsx(next_link["default"], {
            href: `/user/${post.User.id}`,
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              children: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Avatar, {
                children: post.User.nickname[0]
              })
            })
          }),
          title: post.User.nickname,
          description: /*#__PURE__*/jsx_runtime_.jsx(components_PostCardContent, {
            postData: post.content
          })
        })]
      })
    }), commentFormOpened && /*#__PURE__*/(0,jsx_runtime_.jsxs)(jsx_runtime_.Fragment, {
      children: [/*#__PURE__*/jsx_runtime_.jsx(components_CommentForm, {
        post: post
      }), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.List, {
        header: `${post.Comments.length} 댓글`,
        itemLayout: "horizontal",
        dataSource: post.Comments,
        renderItem: item => /*#__PURE__*/jsx_runtime_.jsx("li", {
          children: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Comment, {
            author: item.User.nickname,
            avatar: /*#__PURE__*/jsx_runtime_.jsx(next_link["default"], {
              href: `/user/${item.User.id}`,
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                children: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Avatar, {
                  children: item.User.nickname[0]
                })
              })
            }),
            content: item.content
          })
        })
      })]
    })]
  }, post.id);
};

/* harmony default export */ const components_PostCard = (PostCard);

/***/ })

};
;